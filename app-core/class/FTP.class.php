<?php

//error_reporting(0);

class FTP {

  private $connection;
  private $login;
  private $ftp_server;
  private $ftp_user;
  private $ftp_password;
  private $directoireTravail;
  
  /*Constructeur de la classe*/
  public function FTP($ftp_server,$ftp_user,$ftp_password){
  	$this->connection = ftp_connect($ftp_server) or null;
  	$this->login = ftp_login($this->connection, $ftp_user, $ftp_password) or null;
    $this->ftp_server = $ftp_server;
    $this->ftp_user = $ftp_user;
    $this->ftp_password = $ftp_password;
    $this->directoireTravail = ftp_pwd($this->connection);
  }
  

 // Obtient la connection 
  public function getConnection(){
    return $this->connection;
  }

  // Obtient les infos du login
  public function getLogin() {
    return $this->login;
  }

  public function getFTP_user(){
    return $this->ftp_user;
  }


  // Fonction qui ferme la connection au serveur FTP
  public function close(){
    @ftp_close($this->connection);
  }
  
  // retourne la direccion du dossier actuel
  public function getDirectoireTravail(){
    return $this->directoireTravail;
  }

  // Fonction qui permet télécharger le fichier
  public function getFichier($nomFichier) {
    if ($this->isFichier($nomFichier)) {
      $nomFichier = substr(strrchr($nomFichier, "/"), 1);
      return ftp_get(
        $this->connection,
        '../app-data/users/' . $this->ftp_user . '/' . $nomFichier,
        $nomFichier,
        FTP_BINARY
      );
    }
  }

  // Fonction qui verifie si l'argument reçu est un dossier
  // @return true si l'argument reçu est un dossier || false sinon
  public function isDirectoire($nomDir) {
    return is_dir('ftp://'. $this->ftp_user . ':' . $this->ftp_password . '@' . $this->ftp_server . $nomDir);
  }

  // Fonction qui verifie si l'argument reçu est un fichier
  // @return true si l'argument reçu est un fichier || false sinon
  public function isFichier($nomFichier) {
    return is_file('ftp://'. $this->ftp_user . ':' . $this->ftp_password . '@' . $this->ftp_server . $nomFichier);
  }

  // Fonction qui change l'argument reçu à directorire
  // @return le dossier || null si l'argument n'est pas un dossier
  public function toDir($nomDir) {
    if ($this->isDirectoire($nomDir)) {
      return 'ftp://'. $this->ftp_user . ':' . $this->ftp_password . '@' . $this->ftp_server . $nomDir;
    } else {
      return null;
    }
  }

  public function chargerFichier($nomFichier,$fichier_source) {
    return ftp_put(
        $this->connection,
        $nomFichier,
        $fichier_source,
        FTP_BINARY
      );
  }

  /*Change le dossier de travail*/
  public function changerDirectoireTravail($directoireTravail){
    ftp_chdir($this->connection, $directoireTravail);
    $this->directoireTravail = ftp_pwd($this->connection);
  }

  /* Retourne la location du directoire de travail*/
  public function retournerLocation() {
    // Si l'ubication actuel est une dossier avant root (/)
    if (strrpos($this->directoireTravail, '/') == 0) {
      $directoireAvant = "/";
    } else {
      $directoireAvant = substr($this->directoireTravail,0, strrpos($this->directoireTravail, '/'));
    }
    ftp_chdir($this->connection, $directoireAvant);
    $this->directoireTravail = ftp_pwd($this->connection);
  }

  /* Function qui permet créer un nouveau dossier dans le serveur*/
  public function creerDossier($nomDossier) {
    return ftp_mkdir($this->connection, $nomDossier);
  }


  /* Permet renommer le directoire reçu*/
  public function renommerDirectoire($nomActuel, $nouveauNom){
    ftp_rename($this->connection, $nomActuel, $nouveauNom);
  }
  
  /*Permet supprimer le dossier ou fichier reçu*/
  public function supprimer($fichier) {
    if ($this->isFichier($this->directoireTravail . "/" . $fichier)) {
      return ftp_delete($this->connection, $this->directoireTravail . "/" .$fichier);
    } else {
      return ftp_rmdir($this->connection, $this->directoireTravail . "/" .$fichier);
    }
    
  }

  /*Liste de directoires du dossier de travail*/
  public function dirList(){
    return ftp_nlist($this->connection, $this->directoireTravail);
  }

}
?> 