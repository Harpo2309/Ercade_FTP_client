<?php 


// importing neccesary classes
require('../app-core/class/FTP.class.php');

session_start();
// Si la session n'est pas actif, on montre la page de login
if (!isset($_SESSION['ftp_logged'])) {
  header('Location: login.view.php');
  session_destroy();
  die();
}

// On verifie si la connection n'existe pas pour l'initialiser
if (!isset($ftpConnection)) {
 $ftp_server = $_SESSION['ftp_server'];
 $ftp_user = $_SESSION['ftp_user'];
 $ftp_password = $_SESSION['ftp_password'];
 $ftpConnection = new FTP($ftp_server,$ftp_user,$ftp_password);
}

// Changer le directoire de travail s'il existe déjà un
if (isset($_SESSION['current_path'])) {
  $current_path = $_SESSION['current_path'];
  $ftpConnection->changerDirectoireTravail($current_path);
}


// Fonction qui permet calculer la taille des dossiers
function foldersize($path) {
  $total_size = 0;
  $files = scandir($path);

  foreach($files as $t) {
    if (is_dir(rtrim($path, '/') . '/' . $t)) {
      if ($t<>"." && $t<>"..") {
          $size = foldersize(rtrim($path, '/') . '/' . $t);

          $total_size += $size;
      }
    } else {
      $size = filesize(rtrim($path, '/') . '/' . $t);
      $total_size += $size;
    }
  }
  return $total_size;
}

// Fonction qui permet donner un format au taille du dossier ou fichier
function format_size($size) {
  $mod = 1024;
  $units = explode(' ','B KB MB GB TB PB');
  for ($i = 0; $size > $mod; $i++) {
    $size /= $mod;
  }

  return round($size, 2) . ' ' . $units[$i];
}


// Fermeture de la session
if (isset($_POST['logged_out'])) {
  $ftpConnection->close();
  session_destroy();
  header('Location: login.view.php');
}


//Changer de directoire
if (isset($_POST['access'])) {
  // en gardant la route actuelle
  $directorireTravail = $_POST['access_dir'];
  $ftpConnection->changerDirectoireTravail($directorireTravail);
  $_SESSION['current_path'] = $ftpConnection->getDirectoireTravail();
}

// Retourner un directoire avant
if (isset($_POST['btn_back'])) {
  $ftpConnection->retournerLocation();
  $_SESSION['current_path'] = $ftpConnection->getDirectoireTravail();
}

// Fonction qui permet créer un nouveau dossier
if (isset($_POST['new_dir'])) {
  $folder_name = $_POST['name_new_dir'];
  $ftpConnection->creerDossier($folder_name);
}

//modifier nom du directoire ou fichier
if (isset($_POST['ch_name_dir'])) {
  $data = $_POST['name_dir'];
}

// Fonction permettant de télécharger le fichier
if (isset($_POST['btn_download'])) {
  $file = $_POST['file_name'];
  $download = $ftpConnection->getFichier($file);
  if ($download) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($file).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);
  }
  
}

// Fonction permettant de charger des fichiers au serveur
if (isset($_POST['route'])) {
  $route = $_POST['route'];
  if ($route != "") {
    $name_file = $route . basename($_FILES['btn_upload']['name']);
    $source_file = $_FILES['btn_upload']['tmp_name'];
    $upload = $ftpConnection->chargerFichier($name_file,$source_file);
    $uploaded = ($upload) ? 'true' : 'false'; 
  }
}

// Fonction permettant de supprimer des fichiers ou dossiers du serveur
if (isset($_POST['drop_data'])) {
  $data = $_POST['drop_data'];
  $deleteData = $ftpConnection->supprimer($data);
  $deleted = ($deleteData) ? 'true' : 'false';
}

 ?>