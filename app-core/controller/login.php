<?php 

//error_reporting(0);

// importing neccesary classes
require('../app-core/class/FTP.class.php');

if(
	isset($_POST['ftp_server']) &&
	isset($_POST['ftp_user']) &&
	isset($_POST['ftp_password'])) {
		$ftp_server = $_POST['ftp_server'];
		$ftp_user = $_POST['ftp_user'];
		$ftp_password = $_POST['ftp_password'];
	    $ftpConnection = new FTP($ftp_server,$ftp_user,$ftp_password);

		if ($ftpConnection->getLogin() == null || $ftpConnection->getConnection() == null) {
			$loginError = true;
		} else {
			session_start();
			$_SESSION['ftp_server'] = $ftp_server;
			$_SESSION['ftp_user'] = $ftp_user;
			$_SESSION['ftp_password'] = $ftp_password;
			$_SESSION['ftp_logged'] = 'logged';
			// Creation du dossier backup pour l'utilisateur
			if (!is_dir('../app-data/users/' . $ftp_user)) {
				mkdir('../app-data/users/' . $ftp_user);
			}
			header('Location: index.view.php');
		}
	}
 ?>


