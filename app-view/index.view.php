<?php 

// importing controller
require('../app-core/controller/index.php');

 ?>

<!DOCTYPE>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>Ercade FTP Client</title>
    <!-- Custom CSS files -->
    <link rel="stylesheet" href="src/css/styles.css">
    <link rel="stylesheet" href="src/css/index.css">
    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
    crossorigin="anonymous"></script>
    <!-- Bootstrap 4 Core -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    <!-- SweetAlert2 -->
    <script src="https://unpkg.com/sweetalert2@7.1.0/dist/sweetalert2.all.js"></script>
    <!-- Ionicons (icons) -->
    <link href="lib/ionicons-2.0.1/css/ionicons.min.css" rel="stylesheet">
    <!-- Custom JS files -->
    <script src="src/js/index.js"></script>
</head>
<body>
    <header>
        <img src="src/img/ercade-logo.png" alt="ercade-logo">

    </header>
    <form id="form-menu" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" enctype="multipart/form-data">
        <nav id="nav-menu">
            <ul>
                <!-- Bouton charger fichier au serveur -->
                <li>
                    <label for="btn_upload" class="ion-android-upload">
                        <input type="file" name="btn_upload" id="btn_upload">
                        <!-- hidden pour garder la route actuelle -->
                        <input id="current_path" type="hidden" name="route">
                    </label>
                </li>
                <!-- Bouton "Plus" pour créer un dossier -->
                <li>
                    <span data-toggle="modal" data-target="#modal-new-folder" class="ion-plus-circled"></span>
                </li>
                <!-- Bouton retourner un niveau (Just visible si le route n'est pas "/")-->
                <?php if($ftpConnection->getDirectoireTravail() !== '/'): ?>
                    <li>
                        <input type="submit" id="btn_back" name="btn_back">
                        <label id="label_btn_back" class="ion-android-arrow-back"></label>
                    </li>
                <?php endif; ?>
                <!-- Route actuelle de travail -->
                <li><span id="route"><?php echo $ftpConnection->getDirectoireTravail(); ?></span></li>
                <!-- Bouton fermer la session -->
                <li><span name="log_out" id="log_out" class="ion-power"></span></li>
            </ul>
        </nav>
    </form>
    <div class="container">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <th>Type</th>
                    <th>Nom</th>
                    <th>Taille</th>
                    <th colspan="3">Actions</th>
                </thead>
                <tbody>
                    <?php foreach ($ftpConnection->dirList() as $dir): ?>
                        <form id="form-main" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post">
                            <tr>
                                <!-- Îcone de fichier ou dossier, selon le case -->
                                <td>
                                    <span class="<?php echo $ftpConnection->isDirectoire($dir) ? 'ion-folder' : 'ion-document'; ?>"></span>
                                </td>
                                <!-- Permet acces au dossier, ou montre le nom du fichier -->
                                <td>
                                    <?php if($ftpConnection->isDirectoire($dir)): ?>
                                        <input type="hidden" name="access_dir" value="<?php echo $dir; ?>">
                                        <input type="submit" name="access" class="btn btn-link" value="<?php echo substr(strrchr($dir, "/"), 1); ?>">
                                    <?php else: ?>
                                        <p><?php echo substr(strrchr($dir, "/"), 1); ?></p>
                                    <?php endif; ?>
                                </td>
                                <!-- Montre la taille du dossier || fichier -->
                                <?php if ($ftpConnection->isDirectoire($dir)): ?>
                                    <td><?php echo format_size(foldersize($ftpConnection->toDir($dir))); ?></td>
                                <?php else: ?>
                                    <td><?php echo format_size(ftp_size($ftpConnection->getConnection(), $dir)); ?></td>
                                <?php endif; ?>
                                <!-- Bouton modifier -->
                                <td>
                                    <span onclick="modifier('<?php echo substr(strrchr($dir, "/"), 1); ?>');" data-toggle="modal" data-target="#modal-modif" id="mod_file" class="ion-edit"></span>
                                </td>
                                <!-- Bouton supprimer -->
                                <td>
                                    <span onclick="confirmDelete('<?php echo substr(strrchr($dir, "/"), 1); ?>','<?php echo $ftpConnection->isDirectoire($dir) ? 'dossier' : 'fichier'; ?>');" id="drop_file" class="ion-trash-b"></span>
                                </td>
                                <!-- Bouton télécharger (just pour des fichiers) -->
                                <?php if($ftpConnection->isFichier($dir)): ?>
                                    <td>
                                        <input type="hidden" name="file_name" value="<?php echo $dir; ?>">
                                        <button type="submit" name="btn_download">
                                            <span class="ion-android-download"></span>
                                        </button>
                                       
                                    </td>
                                <?php endif; ?>
                            </tr>
                        </form>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>

        <!-- Modal de modification -->
        <div id="modal-modif" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Modifier nom</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post">
                  <div class="modal-body">
                    <div class="form-group">
                        <label for="name_dir">Saisir nouveau nom :</label>
                        <input class="form-control" type="text" name="name_dir" id="name_dir">
                    </div>
                  </div>
                  <div class="modal-footer">
                        <input type="submit" class="btn btn-primary" name="ch_name_dir" value="Changer nom">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                  </div>
              </form>
            </div>
          </div>
        </div>

        <!-- Modal nouveau dossier -->
        <div id="modal-new-folder" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Nouveau dossier</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post">
                  <div class="modal-body">
                    <div class="form-group">
                        <label for="name_dir">Nom du dossier :</label>
                        <input class="form-control" required type="text" name="name_new_dir" id="name_new_dir">
                    </div>
                  </div>
                  <div class="modal-footer">
                        <input type="submit" class="btn btn-primary" name="new_dir" value="Créer dossier">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                  </div>
              </form>
            </div>
          </div>
        </div>

    </div>
    <footer>
        © <strong>2017 Ercade. All rights reserved.</strong>
    </footer>

    <!-- Messages JS -->

    <!-- Message en cas d'erreur de charge de fichier au serveur-->
    <?php if(isset($uploaded) && $uploaded == 'false'): ?>
        <script>
            $(document).ready(function(){
                swal({
                    title: 'Erreur de chargement',
                    text: 'Veuillez réessayer de charger le fichier',
                    confirmButtonText: 'Accepter',
                    type: 'warning'
                });
            });
        </script>
    <!-- Message en cas de charger correctement le fichier au serveur -->
    <?php elseif(isset($uploaded) && $uploaded == 'true'): ?>
        <script>
            $(document).ready(function(){
                swal({
                    title: 'Fichier chargé au serveur',
                    text: 'Votre fichier a été chargé correctement',
                    confirmButtonText: 'Accepter',
                    type: 'success'
                });
            });
        </script>
    <?php endif; ?>

    <!-- Message en cas de supprimer correctement le fichier ou dossier -->
    <?php if(isset($deleted) && $deleted == 'true'): ?>
        <script>
            $(document).ready(function(){
                swal({
                    title: 'Suppression du serveur',
                    text: 'Votre fichier a été supprimé correctement',
                    confirmButtonText: 'Accepter',
                    type: 'success'
                });
            });
        </script>
    <?php endif; ?>
</body>  
</html>