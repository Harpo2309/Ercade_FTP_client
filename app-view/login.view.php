<?php 

// importing controller
require('../app-core/controller/login.php');

 ?>

 <!DOCTYPE>
 <html lang="fr">
 <head>
 	<meta charset="UTF-8">
 	<meta name="viewport" content="width=device-width initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
 	<title>Ercade FTP Client - Login</title>
 	<!-- Custom CSS files -->
    <link rel="stylesheet" href="src/css/styles.css">
    <link rel="stylesheet" href="src/css/login.css">
 	 <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
    crossorigin="anonymous"></script>
    <!-- Bootstrap 4 Core -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    <!-- SweetAlert2 -->
    <script src="https://unpkg.com/sweetalert2@7.1.0/dist/sweetalert2.all.js"></script>
    <!-- Ionicons (icons) -->
    <link href="lib/ionicons-2.0.1/css/ionicons.min.css" rel="stylesheet">
    <!-- Custom JS files -->
    <script src="src/js/login.js"></script>
 </head>
 <body>
 	<div class="wrap">
 		<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" id="form-login" method="post">
 			<fieldset>
 				<img src="src/img/ercade-logo.png" alt="ercade-logo">
 				<h3>Client FTP</h3>
 				<!-- Adresse IP ou nom du serveur -->
 				<div class="input-group">
				    <span class="input-group-addon">
				    	<span class="input ion-network"></span>
				    </span>
				    <input id="ftp_server" type="text" class="form-control" name="ftp_server" placeholder="Addresse IP ou nom du serveur">
				</div>
				<div class="log">Saisir addresse IP ou nom du serveur</div>
				<!-- Nom d'utilisateur -->
				<div class="input-group">
				    <span class="input-group-addon">
				    	<span class="input ion-person"></span>
				    </span>
				    <input id="ftp_user" type="text" class="form-control" name="ftp_user" placeholder="Utilisateur">
				</div>
				<div class="log">Saisir nom d'utilisateur</div>
				<!-- Mot de pass -->
				<div class="input-group">
				    <span class="input-group-addon">
				    	<span class="input ion-locked"></span>
				    </span>
				    <input id="ftp_password" type="password" class="form-control" name="ftp_password" placeholder="Mot de pass">
				</div>
				<div class="log">Saisir mot de pass</div>
				<div class="input-group">
					<input class="btn btn-block" name="btn_login" id="btn_login" type="button" value="Se connecter">
				</div>
 			</fieldset>
 		</form>

 		<!-- JS for show a login error message -->
 		<?php if(isset($loginError)): ?>
 			<script>
 				$(document).ready(function(){
 					swal({
 						title: 'Erreur d\'autentication',
 						text: 'Veuillez vérifier vos credentials',
 						confirmButtonText: 'Réessayer'
 					});
 				});
 			</script>
 		<?php endif; ?>
 	</div>
 </body>
 </html>