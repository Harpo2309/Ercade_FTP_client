


$(document).ready(function(){

	// Charger des fichiers au serveur
	$('#btn_upload').on("change",function(){
		if ($(this).val() !== '') {
			// insertion au root
			route = ($('#route').html() === '/') ? $('#route').html() : $('#route').html() + '/';
			// preparation du formulaire pour charger le fichier au serveur
			$('#current_path').val(route);
			$('#form-menu').submit();
		}
	});

	// retourner un dossier avant
	$('#label_btn_back').click(function(){
		$('#btn_back').click();
	});

	// Fermer la session
	$('#log_out').click(function(){
		var input = $("<input>")
               .attr("type", "hidden")
               .attr("name", "logged_out").val("true");
        $('#form-menu').append($(input));
		$('#form-menu').submit();
	});
});


// Fonction pour modifier le nom du fichier ou dossier
function modifier(dir) {
	$('#name_dir').val(dir);
}

// Fonction permettant de confirmer la suppresion du fichier ou dossier
function confirmDelete(name,type) {
	swal({
	  title: 'Supprimer du serveur',
	  text: "vous êtes sûr de supprimer le " + type + " " + name + " ?",
	  type: 'question',
	  showCancelButton: true,
	  cancelButtonText: 'Annuler',
	  confirmButtonText: 'Supprimer'
	}).then((result) => {
	  if (result.value) {
	  	var input = $("<input>")
               .attr("type", "hidden")
               .attr("name", "drop_data").val(name);
        $('#form-main').append($(input));
        $('#form-main').submit();
	  }
	});
}

