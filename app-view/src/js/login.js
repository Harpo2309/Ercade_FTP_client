
$(document).ready(function(){

	// Actions login button
	$('#btn_login').click(function() {
		var arr_form_control = $('.form-control'); // input types
		var arr_input_group = $('.input-group'); // div containers
		var arr_log = $('.log'); // div log messages

		// Validation des inputs
		for(i = 0; i < arr_form_control.length; i ++) {
			arr_log[i].style.display = 'none';
			if (arr_form_control[i].value.trim().length === 0) {
				arr_form_control[i].value = '';
				arr_log[i].style.display = 'inline';
			}
		}

		if(!erreurs(arr_log)) {
			$('#form-login').submit();
		}

	});

	function erreurs(arr_log) {
		for(i = 0; i < arr_log.length; i ++) {
			if (arr_log[i].style.display === 'inline') {
				return true;
			}
		}
		return false;
	}
});
